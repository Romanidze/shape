package com.romanidze.shape.fixed.blackbox;

import com.romanidze.shape.ShapeClassifierFixed;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

public class TriplesBlackBoxTest {

  private ShapeClassifierFixed shapeClassifier;

  @BeforeEach
  public void initialize() {
    shapeClassifier = new ShapeClassifierFixed();
  }

  @ParameterizedTest(name = "#{index} - Test with input = {0} and expected result = {1}")
  @CsvFileSource(resources = "/triples.csv", numLinesToSkip = 1)
  public void testTriples(String input, String expected) {
    shapeClassifier.evaluateGuess(input);
    Assertions.assertEquals(expected, shapeClassifier.evaluateGuess(input), "input: " + input);
  }
}
