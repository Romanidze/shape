package com.romanidze.shape.original.whitebox;

import com.romanidze.shape.ShapeClassifier;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

public class OriginalWhiteBoxTest {

  private ShapeClassifier shapeClassifier;

  @BeforeEach
  public void initialize() {
    shapeClassifier = new ShapeClassifier();
  }

  @ParameterizedTest(name = "#{index} - Test with input = {0} and expected result = {1}")
  @CsvFileSource(resources = "/whitebox.csv", numLinesToSkip = 1)
  public void testWhiteBox(String input, String expected) {
    shapeClassifier.evaluateGuess(input);
    Assertions.assertEquals(expected, shapeClassifier.evaluateGuess(input), "input: " + input);
  }
}
